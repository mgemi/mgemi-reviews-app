# Product Reviews

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5f0fe1fea2194fb98154467f6331d0ad)](https://www.codacy.com/app/jaredwilli/mgemi-reviews-app?utm_source=mgemi@bitbucket.org&amp;utm_medium=referral&amp;utm_content=mgemi/mgemi-reviews-app&amp;utm_campaign=Badge_Grade)

Product reviews is a separate application located in the product-reviews-app repository and is developed independently from the demandware-mgemi repo. It can be run from `localhost:3000` making it very easy to get up and running when you want to set it up for development. Below are the steps for doing so, as well as how to test, build, and consume it in the demandware app.

## Step-by-step guide

First, clone the repo and install the dependencies

`git clone git@bitbucket.org:mgemi/mgemi-reviews-app.git && cd mgemi-reviews-app && npm install`

Copy the .env.local.example file and name it `.evn.local`

In .env.local replace `<your_authorization_token_value_here>` with the following token:

`ol8q6Xdu5rSbZ59vXkpHOPMWAMPkwWPE2VP`

## Running the app locally

You can run the app on localhost:3000 which will be running with the development environment variable that is built into Create React App. When the ENV is set to development the TurnTo api request calls are made to a proxy server that will proxy requests with the Authorization Bearer token so that the response data will return properly, otherwise the request would fail.

Run the app with:

`npm start`

## Running unit tests

To run the unit tests use the command:

`npm test`

or

`npm test --watch`

## Building and deploying

The reviews app must be built in order for the demandware app to be able to pull it in. In the pdp.js file in app-mgemi the JS and CSS files from the build folder of the product-reviews repo is required, and that is how it outputs the reviews app.

The demandware app has a package.json file which is where the definition of the product-reviews app is and the version number it installs. The version is the tag number that is cut for each new update made to the reviews app. 

Once you have made a change to the reviews app, to package it up for the demandware app to be able to consume, you must  do the following:

Update the version number of the package.json in product-reviews app. (follow semantic versioning method).

Then run the following commands:

`npm run build`

`git add .`

`git commit -am 'somee commit message - built files included'`

`git push origin <YOUR BRANCH>`

Once your branch is merged into master, you can then update your local master branch:

```
git checkout master
git fetch origin 
git merge --ff-only origin/master
```

Then create a new tag for the release: (use the same version number as package.json +1 digit)

`git tag -a 0.1.97 -m 'some release message'`

`git push origin 0.1.97`

__NOTE: In order to keep things from being confusing and to be able to track them separately, make sure the tag version number is 1 digit ahead of the package.json version number. This will lead to less issues trust me.

Once that is completed, you can go to the demandware-mgemi app and bump the version number in package.json for the product-reviews dependency.

`npm install`


Finally, rebuild the demandware app, and upload the cartridges to your SIG instance and you should see your changes to the reviews app in your dev instance.
