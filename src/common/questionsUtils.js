import { isDev } from './utils';

/**
 * createQuestion
 *
 * @description Creates a new Q&A question in the TurnTo system.
 * @param {Object} data the data object of the form submission. Includes text, name, and email
 */
export const createQuestion = data => {
  const dataObj = { method: 'POST' };

  // Mens sku '00_1994_01';
  // Womens sku '03_1000_22';
  // No catalog found: 00_2978_01
  // Essato: 00_1222_14
  const sku =
    window.app && window.app.page && !isDev ? window.app.page.id : '03_1000_22';

  let url = '';

  // Handle things differently for devevlopment
  if (isDev) {
    let catalogItems = [];
    catalogItems.push({
      sku: sku
    });

    url = `https://anticors-anywhere.herokuapp.com/https://api.turnto.com/v1.2/questions?catalogItems=${catalogItems}&text=${
      data.text
    }&name=${data.name}&email=${data.email}`;

    dataObj.headers = {
      Authorization: `Bearer ${process.env.REACT_APP_AUTHORIZATION_BEARER}`
    };
  } else {
    const paramMap = `sku=${sku}&text=${data.text}&name=${data.name}&email=${
      data.email
    }`;

    url = `/s/mgemi/submit-question/?api=questions&parametermap=${encodeURIComponent(
      paramMap
    )}`;
  }

  return fetch(url, dataObj)
    .then(res => res.json())
    .then(json => (isDev ? { data: json } : json))
    .catch(err => (isDev ? console.error(err.message) : null));
};
