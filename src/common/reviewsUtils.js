import { isDev } from './utils';

/**
 * requestReviews
 *
 * @description Builds the request url and data object to send for the request whether it's development or production.
 * @param {Object} pagingObj object containing the pageSize and offset
 */
export const requestReviews = pagingObj => {
  // TODO: Make this more reusable for questions and reviews requests
  const pagingParams = `&limit=${pagingObj.pageSize}&offset=${
    pagingObj.offset
  }`;

  const dataObj = { method: 'GET' };

  // Mens sku '00_1994_01';
  // Womens sku '03_1000_22';
  // No catalog found: 00_2978_01
  // Essato: 00_1222_14
  const sku =
    window.app && window.app.page && !isDev ? window.app.page.id : '03_1000_22';

  const includeRelated = true;
  let url = '';

  // Handle things differently for devevlopment
  if (isDev) {
    url = `https://anticors-anywhere.herokuapp.com/https://api.turnto.com/v1.2/reviews?sku=${sku}&includeRelated=${includeRelated}${pagingParams}`;

    dataObj.headers = {
      Authorization: `Bearer ${process.env.REACT_APP_AUTHORIZATION_BEARER}`
    };
  } else {
    const paramMap = `sku=${sku}&includeRelated=${includeRelated}${pagingParams}`;

    url = `/s/mgemi/turntoget/?api=reviews&parametermap=${encodeURIComponent(
      paramMap
    )}`;
  }

  return fetch(url, dataObj)
    .then(res => res.json())
    .then(json => (isDev ? { data: json } : json))
    .catch(err => (isDev ? console.error(err.message) : null));
};
