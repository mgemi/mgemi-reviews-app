const MONTHS = [
  'Jan.',
  'Feb.',
  'Mar.',
  'Apr.',
  'May',
  'June',
  'July',
  'Aug.',
  'Sept.',
  'Oct.',
  'Nov.',
  'Dec.'
];

/**
 * isDev
 *
 * Check if this is development or test environment obv
 */
export const isDev =
  process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test';

/**
 * formatDate
 *
 * @param {Date} timeStamp
 */
export const formatDate = timeStamp =>
  `${MONTHS[new Date(timeStamp).getMonth()]} ${new Date(
    timeStamp
  ).getDate()}, ${new Date(timeStamp).getFullYear()}`;

/**
 * Check if on localhost.
 */
export const isLocalHost = () => window.location.hostname === 'localhost';
