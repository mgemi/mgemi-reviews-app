/*
     ___,    ___     _,_
    {O,0}   {-.-}   {o,O}
    |)__)   |)~(|   (~~(|
,----"-"-----"-"-----"-"-----,
)   --    --    --     --   _(
\_,------------------------.__)
*/
import React from 'react';

import ReviewsList from '../containers/ReviewsList';
import QuestionList from '../containers/QuestionList';

import Tabs from './Tabs';
import Pane from './Pane';

import './App.css';

const App = () => {
  return (
    <Tabs selected={0}>
      <Pane label="Reviews">
        <ReviewsList />
      </Pane>

      <Pane label="Q&A">
        <QuestionList />
      </Pane>
    </Tabs>
  );
};

export default App;
