import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './FloatingLabel.css';

/** Floating label input field */
const FloatingLabel = ({
  autoComplete,
  error,
  htmlId,
  name,
  value,
  isDisabled,
  onChange,
  placeholder,
  required,
  type,
  width,
  height
}) => {
  const inputClasses = classNames('fl-input', {
    'fl-valid': value && !error,
    'fl-invalid': value && error
  });

  const textareaClasses = classNames('fl-textarea', {
    'fl-valid': value && !error,
    'fl-invalid': value && error
  });

  const errMsgClasses = classNames({
    'fl-error-msg': error,
    'fl-error-show': error
  });

  const style = {
    width: width && width,
    height: height && height
  };

  if (type === 'textarea') {
    return (
      <div className="fl-textarea-container">
        <textarea
          id={htmlId}
          type={type}
          name={name || htmlId}
          style={style}
          className={textareaClasses}
          disabled={isDisabled}
          onChange={onChange}
          autoComplete={autoComplete ? 'on' : 'off'}
          value={value}
        />

        <label className="fl-label" htmlFor={htmlId}>
          {required && <span className="required-indicator">*</span>}

          <span>{placeholder}</span>
        </label>

        {error && <span className={errMsgClasses}>{error}</span>}
      </div>
    );
  }

  return (
    <div className="fl-input-container">
      <input
        id={htmlId}
        type={type}
        name={name || htmlId}
        style={style}
        className={inputClasses}
        disabled={isDisabled}
        onChange={onChange}
        autoComplete={autoComplete ? 'on' : 'off'}
        value={value}
      />

      <label className="fl-label" htmlFor={htmlId}>
        {required && <span className="required-indicator">*</span>}

        <span>{placeholder}</span>
      </label>

      {error && <span className={errMsgClasses}>{error}</span>}
    </div>
  );
};

FloatingLabel.defaultProps = {
  autoComplete: false,
  isDisabled: false,
  resize: 'none',
  required: false
};

FloatingLabel.propTypes = {
  /** Should it autocomplete or not */
  autoComplete: PropTypes.bool,

  /** Error message to display */
  error: PropTypes.string,

  /** ID of the field */
  htmlId: PropTypes.string.isRequired,

  /** Function to call onChange */
  onChange: PropTypes.func.isRequired,

  /** Input name. Recommend setting this to match object's property so a single change handler can be used. */
  name: PropTypes.string,

  /** Value of the field */
  value: PropTypes.any,

  /** Is it disabled or not */
  isDisabled: PropTypes.bool,

  /** The placeholder text or label text to float */
  placeholder: PropTypes.string.isRequired,

  /** Is this a required field */
  required: PropTypes.bool,

  /** The input type to use */
  type: PropTypes.string.isRequired,

  /** The width of the form field */
  width: PropTypes.string,

  /** The height of the form field */
  height: PropTypes.string,

  /** Resize value or none */
  resize: PropTypes.string
};

export default FloatingLabel;
