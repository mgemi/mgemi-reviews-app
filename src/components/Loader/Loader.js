import React from 'react';
import PropTypes from 'prop-types';

const Loader = props => {
  return (
    <svg
      width="16px"
      height="16px"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle
        r="45"
        cx="50"
        cy="50"
        fill="none"
        strokeWidth="10"
        transform="rotate(276 50 50)"
        stroke={props.variant === 'dark' ? '#29292A' : '#FFFFFF'}
        strokeDasharray="212.05750411731105 72.68583470577035"
      >
        <animateTransform
          dur="1s"
          begin="0s"
          type="rotate3d"
          keyTimes="0;1"
          calcMode="linear"
          repeatCount="indefinite"
          attributeName="transform"
          values="0 50 50;360 50 50"
        />
      </circle>
    </svg>
  );
};

Loader.defaultProps = {
  variant: 'dark'
};

Loader.propTypes = {
  variant: PropTypes.string
};

export default Loader;
