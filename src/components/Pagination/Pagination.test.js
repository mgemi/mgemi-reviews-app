import React from 'react';
import renderer from 'react-test-renderer';
import Pagination from './Pagination';

describe('Pagination', () => {
  const total = 41;

  const onChangePage = jest.fn(() => {
    return Promise.resolve('cool!');
  });

  it('renders the component correctly', () => {
    const tree = renderer
      .create(<Pagination total={total} onChangePage={() => onChangePage()} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  // TODO: test the size prop more
  // it('renders the component correctly', () => {
  //     const tree = renderer.create(<Pagination />).toJSON();
  //     expect(tree).toMatchSnapshot();
  // });
});
