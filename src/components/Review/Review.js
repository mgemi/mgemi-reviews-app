import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';

import { formatDate, isLocalHost } from '../../common/utils';
import { SIZE_OPTIONS, WIDTH_OPTIONS } from '../../constants';

import Rating from '../Rating';
import Tooltip from '../Tooltip';
import LikertScale from '../LikertScale';

import './Review.css';

const Review = class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.props.items
    };
  }

  componentWillReceiveProps(nextProps) {
    const { items } = nextProps;
    items.forEach((item, index) => {
      if (index !== 0) {
        item.expanded = false;
      }
    });

    if (items) {
      this.setState({
        items: items
      });
    }
  }

  handleOpenChat = () => {
    if (window.app && window.app.cta) {
      window.app.cta.openChatNow();
    }
  };

  toggle = index => {
    const { items } = this.state;

    items[index].expanded = !this.state.items[index].expanded;

    this.setState({ items });
  };

  render() {
    const { props } = this;
    const { items } = this.state;

    // TODO: Make this a return a sign block of html for both product details and reviewer-details
    const getProductDetail = item => {
      const hasMaterial = _.get(
        item,
        'catalogItems[0].attributes[0].values[0]'
      );

      return (
        <div className="review-product-detail">
          <span className="shoe">{item.catalogItems[0].title}</span>
          {/* <span className="color"> in Charcoal Gray </span> */}
          {hasMaterial && (
            <span className="texture">
              {' '}
              in {item.catalogItems[0].attributes[0].values[0].value}
            </span>
          )}
        </div>
      );
    };

    const review = props.items.map((item, index) => {
      // TODO: Make this a configurable setting
      const domain = isLocalHost() ? 'gmail.com' : 'mgemi.com';

      const isCollapsed = index !== 0 && !items[index].expanded;
      const isFitExpert = item.user.emailAddress.split('@')[1] === domain;

      return (
        <li
          key={item.id}
          id={`review-${item.id}`}
          className={`review-item ${isCollapsed ? 'collapsed' : 'expanded'}`}
        >
          <div className="review-content">
            <div className="review-details">
              <Rating rating={item.rating} size={20} />

              <h3 className="review-title">{item.title}</h3>
            </div>

            <div className="review-description">
              <p>{item.text}</p>

              <div className="review-meta desktop">
                {getProductDetail(item)}

                <div className="reviewer-detail">
                  {item.user.nickName && (
                    <span className="name">
                      {item.user.nickName.toLowerCase()}
                    </span>
                  )}

                  {item.user.city &&
                    item.user.state && (
                      <span className="location">
                        {item.user.city.toLowerCase()},{' '}
                        {item.user.state.toLowerCase()}
                      </span>
                    )}

                  {item.dateCreated && (
                    <span className="date">{formatDate(item.dateCreated)}</span>
                  )}

                  {isFitExpert && (
                    <span className="fit-expert">
                      <Tooltip
                        label="M.Gemi Fit Expert"
                        callback={() => this.handleOpenChat()}
                        click
                      >
                        Our fit experts try on and wear test every shoe we
                        release. They are committed to providing feedback on
                        fit, comfort and quality. Have a question? We can help
                        you find the right style and fit for your specific
                        needs.
                        <a
                          className="chat-link"
                          style={{
                            display: 'block',
                            color: '#fff',
                            textDecoration: 'underline',
                            textTransform: 'uppercase',
                            marginTop: '20px'
                          }}
                        >
                          Chat with a Fit Expert
                        </a>
                      </Tooltip>
                    </span>
                  )}
                </div>
              </div>
            </div>

            <div className="review-meta mobile">
              {getProductDetail(item)}

              <div className="reviewer-detail">
                {item.user.nickName && (
                  <span className="name">
                    {item.user.nickName.toLowerCase()}
                  </span>
                )}

                {item.user.city &&
                  item.user.state && (
                    <span className="location">
                      {item.user.city.toLowerCase()},{' '}
                      {item.user.state.toLowerCase()}
                    </span>
                  )}

                {item.dateCreated && (
                  <span className="date">{formatDate(item.dateCreated)}</span>
                )}

                {isFitExpert && (
                  <span className="fit-expert">
                    <Tooltip
                      label="M.Gemi Fit Expert"
                      callback={() => this.handleOpenChat()}
                      click
                    >
                      Our fit experts try on and wear test every shoe we
                      release. They are committed to providing feedback on fit,
                      comfort and quality. Have a question? We can help you find
                      the right style and fit for your specific needs.
                      <a
                        className="chat-link"
                        style={{
                          display: 'block',
                          color: '#fff',
                          textDecoration: 'underline',
                          textTransform: 'uppercase',
                          marginTop: '20px'
                        }}
                      >
                        Chat with a Fit Expert
                      </a>
                    </Tooltip>
                  </span>
                )}
              </div>
            </div>

            <div className="review-fit">
              {item.dimensions.map((dimension, index) => {
                if (dimension.dimensionLabel === 'Size') {
                  return (
                    <LikertScale
                      key={index}
                      value={dimension}
                      options={SIZE_OPTIONS}
                    />
                  );
                } else if (dimension.dimensionLabel === 'Width') {
                  return (
                    <LikertScale
                      key={index}
                      value={dimension}
                      options={WIDTH_OPTIONS}
                    />
                  );
                }
              })}
            </div>
          </div>

          {index !== 0 && (
            <div className="read-more">
              <button
                type="button"
                className="read-more-btn"
                onClick={() => this.toggle(index)}
              >
                <span className="more">Read More</span>
                <span className="less">Read Less</span>

                <svg
                  className="arrow-icon"
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="7"
                  viewBox="0 0 16 7"
                >
                  <path
                    fill="none"
                    fillRule="evenodd"
                    stroke="#29292A"
                    d="M0 1l8 5 8-5"
                  />
                </svg>
              </button>
            </div>
          )}
        </li>
      );
    });

    return <ul className="list-reviews">{review}</ul>;
  }
};

Review.propTypes = {
  /** The list of review items to output */
  items: PropTypes.array.isRequired
};

export default Review;
