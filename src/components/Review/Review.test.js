import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import * as setupTests from '../../setupFiles';
import fetchMock from 'fetch-mock';

import Review from '../Review';
import { reviewData } from '../../config/data';

describe('Review component', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('renders without crashing', () => {
    shallow(<Review items={reviewData.reviews} />);
  });

  it('renders the component correctly', () => {
    const tree = renderer
      .create(<Review items={reviewData.reviews} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
