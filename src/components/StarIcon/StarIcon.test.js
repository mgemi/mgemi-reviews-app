import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import * as setupTests from '../../setupFiles';

import StarIcon from '../StarIcon';

describe('StarIcon component', () => {
  const wrapper = mount(<StarIcon />);

  it('renders without crashing', () => {
    mount(<StarIcon />);
  });

  it('renders the component correctly', () => {
    const tree = renderer.create(<StarIcon />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders the correct HTML', () => {
    expect(wrapper.find('svg').first()).toHaveClassName('star-icon');
    expect(wrapper.find('path').length).toBe(2);
  });

  // TODO: test the size prop more
  it('renders the component correctly', () => {
    const tree = renderer.create(<StarIcon size={20} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
