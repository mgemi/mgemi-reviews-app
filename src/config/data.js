/* Just some mock data dude */

export const reviewData = {
  offset: 0,
  limit: 50,
  total: 102,
  reviews: [
    {
      id: 1023,
      rating: 2,
      title: 'Favorite shoes!!!!',
      text:
        'I have four pairs of these moccs. I need to replace my black pair soon. They are my favorite go to shoes. Comfortable and can be somewhat dressed up or down. Great for running around in. Like wearing slippers. They seem to last quite a while too. Excellent quality.',
      textLength: 265,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22', // window.app.page.id
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Wide']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-26T00:17:30-05:00',
      user: {
        nickName: 'Jaclyn M.',
        firstName: 'Jaclyn',
        lastName: 'Marion',
        emailAddress: 'redpenguin7@hotmail.com',
        externalId: null,
        city: 'Coventry',
        state: 'CT',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 1018,
      rating: 5,
      title: 'Best shoe ever!',
      text:
        'The Felize is probably the most comfortable shoe I have worn.  The style is one of soft comfort yet the sturdy structure is very impressive in a lightweight shoe.  The Felize slips on easily and the shoe just molds to your foot.  I am on my second pair and the colors are simply scrumptious!',
      textLength: 291,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Full Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Wide']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-25T16:26:46-05:00',
      user: {
        nickName: 'Patricia D.',
        firstName: 'Patricia',
        lastName: 'Dore',
        emailAddress: 'pdore8011@aol.com',
        externalId: null,
        city: 'Decatur',
        state: 'GA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 1017,
      rating: 3,
      title: 'Love them!',
      text:
        'I love my felize pair. They’re super comfortable and you don’t even need to break them in. A little slippery when wet out so save them for nice days.',
      textLength: 149,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-25T12:25:09-05:00',
      user: {
        nickName: 'Meredith M.',
        firstName: 'Meredith',
        lastName: 'Mitnick',
        emailAddress: 'meredithmitnick@gmail.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 1008,
      rating: 4,
      title: 'Perfect shoe!!',
      text:
        "Love this shoe! I have it in 4 different colors, and it breaks in after only about a week's worth of wear, and fits like a glove.  The style goes with everything......  Craftsmanship and quality are top notch",
      textLength: 208,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-23T18:29:25-05:00',
      user: {
        nickName: 'CERISE S.',
        firstName: 'CERISE',
        lastName: 'SEMRINEC',
        emailAddress: 'semrinec@charter.net',
        externalId: null,
        city: 'Grand Haven',
        state: 'MI',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 1007,
      rating: 2,
      title: 'Love my Felize',
      text:
        'I love my Felize shoes!   since this was my first pair of MGemi shoes I ordered the wrong size but the excellent customer service agent sent me the next size right away and the are perfect. They are soft and comfortable and I get compliments each time I wear them.  I will definitely order these again.',
      textLength: 302,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: '2017-11-13T00:00:00-05:00',
      dateCreated: '2018-02-23T15:09:52-05:00',
      user: {
        nickName: 'Jeanne M.',
        firstName: 'Jeanne',
        lastName: 'McCabe',
        emailAddress: 'jeannemccabe@gmail.com',
        externalId: null,
        city: 'Morristown',
        state: 'NJ',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 1000,
      rating: 5,
      title: 'Love these shoes!',
      text:
        'These shoes are extremely comfortable and look really nice.  Extra bonus that they come with a personalized card and dust bag for carrying.  Looking forward to buying my next pair!',
      textLength: 180,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-23T11:35:36-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-23T01:26:27-05:00',
      user: {
        nickName: 'Andrea L.',
        firstName: 'Andrea',
        lastName: 'Loh',
        emailAddress: 'aloh@umich.edu',
        externalId: null,
        city: 'SAN FRANCISCO',
        state: 'CA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 999,
      rating: 5,
      title: 'Love, Love, Love',
      text:
        'This shoe is fabulous. Not only is it beautifully designed and fabricated, it is extremely comfortable and looks beautiful on your foot. Every time I wear them I receive compliments.\nI  love this shoe so much, I recently purchased 5 pairs. Once you try them you’ll be hooked as well. \nP.S.  Their Customer Service department also rocks - friendly and helpful. Packaging  is also pristine and they ship out immediately. So glad I discovered this brand.\n\nDebbie K.',
      textLength: 462,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-23T11:35:44-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-23T01:00:45-05:00',
      user: {
        nickName: 'Steven A.',
        firstName: 'Steven',
        lastName: 'Alessio',
        emailAddress: 'debbie.koenigsberg@gmail.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 989,
      rating: 5,
      title: 'Love them',
      text:
        'I am truly happy with my Feliz shoes. Most comfortable pair of shoes I own!',
      textLength: 75,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-21T17:52:40-05:00',
      user: {
        nickName: 'John M.',
        firstName: 'John',
        lastName: 'Marenco',
        emailAddress: 'djmarenco@yahoo.com',
        externalId: null,
        city: 'Suffield',
        state: 'CT',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 971,
      rating: 5,
      title: 'M gemi equals perfect shoe shopping',
      text:
        'I love m gemi in every way. The fashionable styles in vibrant colors. The fitting advice. The comfort and quality of the product. The beauty and personality of the packaging. The simple way they introduce new designs every week. Is there another shoe brand? Not anymore .',
      textLength: 271,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-20T18:07:47-05:00',
      user: {
        nickName: 'beth c.',
        firstName: 'beth',
        lastName: 'courtney',
        emailAddress: 'bcourtney@seigenthaler.com',
        externalId: null,
        city: 'Nashville',
        state: 'TN',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 962,
      rating: 5,
      title: 'Beautiful Italian shoes!!!',
      text:
        "From the careful packaging and shoe bag to the wonderful aroma of fine leather and artisanal shoemaking that greeted me when I opened the box, I was completely smitten! These are by far the nicest shoes (including Ferragamo) that I've bought in a long time. Fit perfectly and luxurious materials. Will be buying more!",
      textLength: 317,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T13:58:10-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-20T11:08:18-05:00',
      user: {
        nickName: 'sylvia c.',
        firstName: 'sylvia',
        lastName: 'calabrese',
        emailAddress: 'sylvia.calabrese@mac.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 960,
      rating: 5,
      title: 'comfy',
      text:
        "These shoes are very comfortable and durable.\nMy daughter saw mine and loved them, so I bought another pair.\nYou won't be disappointed!",
      textLength: 135,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:16:54-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-20T08:47:55-05:00',
      user: {
        nickName: 'CHRISTINE M.',
        firstName: 'CHRISTINE',
        lastName: 'MARKEY',
        emailAddress: 'tinemarkey@gmail.com',
        externalId: null,
        city: 'ASPEN',
        state: 'CO',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 954,
      rating: 4,
      title: 'Nice, but need more cush.',
      text:
        'I love the look of the Felize shoe.  It’s comfortable to an extent. However, it needs a little more cushion on the inside.  I have morton’s neuroma on one of my feet and it is terribly irritated after wearing this shoe for a few hours. I had to address my own cushion inside.',
      textLength: 275,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Wide']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:17:09-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T20:53:28-05:00',
      user: {
        nickName: 'Gena C.',
        firstName: 'Gena',
        lastName: 'Cofer',
        emailAddress: 'genacofer@gmail.com',
        externalId: null,
        city: 'Edisto Island',
        state: 'SC',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 953,
      rating: 4,
      title: 'Wife loves them!',
      text:
        'Got these as a gift for my wife. Ran a half size big, so we did an exchange. Process was simple and fast. Customer service is great. She says they’re very comfortable and is very happy with them.',
      textLength: 195,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T09:59:16-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T19:52:11-05:00',
      user: {
        nickName: 'Roger W.',
        firstName: 'Roger',
        lastName: 'Wong',
        emailAddress: 'baronvonwong@gmail.com',
        externalId: null,
        city: null,
        state: null,
        country: null,
        ageRange: null,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 952,
      rating: 4,
      title: 'Comfy shoes',
      text:
        'The shoes are light, comfortable, almost true the size. I do recommend and would like another lighter color, but need to wait for a big sale.A bit pricy.',
      textLength: 153,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T09:59:33-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T17:48:52-05:00',
      user: {
        nickName: 'Lana L.',
        firstName: 'Lana',
        lastName: 'Levi',
        emailAddress: 'lanallevi@gmail.com',
        externalId: null,
        city: 'Los Angeles',
        state: 'CA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 951,
      rating: 5,
      title: 'Classy flat',
      text:
        'I love the look and feel of this shoe. This shoe is both stylish and comfortable. I am definitely going to look into buying another pair in a different color.',
      textLength: 158,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:00:14-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T17:27:23-05:00',
      user: {
        nickName: 'Colleen M.',
        firstName: 'Colleen',
        lastName: 'McCafferty',
        emailAddress: 'mccaffertyfamily04@yahoo.com',
        externalId: null,
        city: 'Westlake',
        state: 'OH',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 947,
      rating: 5,
      title: 'Love these shoes',
      text:
        'My daughter recommended these shoes \nAbsolutely amazing \nVery comfortable excellent quality molds to your foot \nWill continue to purchase more',
      textLength: 142,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:17:13-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T14:02:35-05:00',
      user: {
        nickName: 'Dee B.',
        firstName: 'Dee',
        lastName: 'Blythe',
        emailAddress: 'deeblythe1185@gmail.com',
        externalId: null,
        city: 'Horseshoe Bay',
        state: 'TX',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 933,
      rating: 5,
      title: 'Love these shoes!',
      text:
        'If I could I would buy these in every color!  I have two pairs now and absolutely love them compared to a similar style in a brand that cost more than double the price!  The extra cushion provides all day comfort, and they perfectly mold to my narrow feet and I never get blisters not even on day one of wear!  As long as new colors keep coming I will keep buying these shoes!',
      textLength: 376,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:18:06-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-18T22:16:29-05:00',
      user: {
        nickName: 'Rachel B.',
        firstName: 'Rachel',
        lastName: 'Beveridge',
        emailAddress: 'rebeveridge@gmail.com',
        externalId: null,
        city: 'Lynbrook',
        state: 'NY',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 921,
      rating: 5,
      title: 'Fabulous Loafer',
      text:
        "I have been looking for a comfortable and stylish loafer for a very long time.  I finally found it with the M. Gemi Felize.  So comfy I can wear it all day.  I'll be buying more of these in other colors!",
      textLength: 203,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-18T16:31:17-05:00',
      user: {
        nickName: 'Susan B.',
        firstName: 'Susan',
        lastName: 'Browning',
        emailAddress: 'susan.browning@me.com',
        externalId: null,
        city: 'Dallas',
        state: 'TX',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 907,
      rating: 5,
      title: 'Runs small',
      text:
        'Love the shoe, but had to send back for a full next size larger. Runs very small. Once you have the correct size, very comfortable. Shoe style is great.',
      textLength: 152,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Full Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-18T05:25:36-05:00',
      user: {
        nickName: 'Diane H.',
        firstName: 'Diane',
        lastName: 'Hombach',
        emailAddress: 'hombachs@icloud.com',
        externalId: null,
        city: 'Schenectady',
        state: 'NY',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 890,
      rating: 5,
      title: 'Comfortable but unstructured',
      text:
        'I love these shoes but find them to be almost too soft and unstructured.  I feel as though they need more support to be able to wear them all day.  The suede is high quality but the toes show wear quickly.  I size up as recommended and find them to be a bit long but still better than my usual Mgemi size.  I appreciate your size recommendations for each shoe.',
      textLength: 360,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-17T16:28:08-05:00',
      user: {
        nickName: 'Cheryl S.',
        firstName: 'Cheryl',
        lastName: 'Sirignano',
        emailAddress: 'cherylsirignano@gmail.com',
        externalId: null,
        city: 'Haverhill',
        state: 'MA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 885,
      rating: 5,
      title: 'My go-to shoe',
      text:
        'The Felize has become my go-to shoe for everyday activities - I have four pairs!  They are extremely comfortable and I love the selection of colors.  They are also beautifully made, like all of the M. Gemi shoes I have.  Love this brand.',
      textLength: 237,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-17T15:39:32-05:00',
      user: {
        nickName: 'Patty A.',
        firstName: 'Patty',
        lastName: 'Armacost',
        emailAddress: 'whiskercreek@verizon.net',
        externalId: null,
        city: 'Pasadena',
        state: 'MD',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 878,
      rating: 4,
      title: 'Casual and comfy',
      text:
        "I decided to order a half size larger than I normally would and I'm glad I did. The fit is very comfortable even my somewhat wide foot. If you are looking for a comfy moccasin type shoe...this one is perfect. The quality seems to be very good. I will be interested to see how they hold up as I can be pretty hard on my shoes. So far so good after several wears.",
      textLength: 361,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-17T14:52:26-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-17T11:52:04-05:00',
      user: {
        nickName: 'Marie A.',
        firstName: 'Marie',
        lastName: 'Akin',
        emailAddress: 'makin54@yahoo.com',
        externalId: null,
        city: 'Mesa',
        state: 'AZ',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 826,
      rating: 5,
      title: 'LOVE THESE SHOES!',
      text:
        "Love these shoes.  Bought them in black.  They are THE most absolutely comfortable shoe I have ever worn.  I'm buying them in every color!",
      textLength: 138,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-16T15:57:50-05:00',
      user: {
        nickName: 'C B.',
        firstName: 'C',
        lastName: 'Buonpastore',
        emailAddress: 'cmbccrn@aol.com',
        externalId: null,
        city: 'wayne',
        state: 'NJ',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 806,
      rating: 5,
      title: 'My new favorite shoe',
      text:
        'Love these shoes! I followed the size chart & fit was great! Had a little concern about something on my first pair but company was great getting a replacement pair sent our immediately. Quality & fit was super! Hope they come back out with plain leather version next',
      textLength: 266,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T14:51:40-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T13:37:00-05:00',
      user: {
        nickName: 'Cheryl K.',
        firstName: 'Cheryl',
        lastName: 'Koenig',
        emailAddress: 'cckdesigns@comcast.net',
        externalId: null,
        city: null,
        state: 'FL',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 779,
      rating: 3,
      title: 'Cute but small and too expensive!',
      text:
        "This shoe is really cute.  I love the ad.  I waited until it went on sale to buy a pair because they are incredibly expensive!  Unfortunately, I am disappointed because they are slightly too short and pretty narrow through the toe, and therefore not the super comfortable shoe that I expected.  It's too late for me to return them, even though I haven't worn them, because I was so excited about having them that I discarded the paperwork for returning them.  I wish I had slowed down and put aside my distaste for returning things by mail and reordering.",
      textLength: 555,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Narrow']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['NO']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T14:53:33-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T11:27:40-05:00',
      user: {
        nickName: 'Crystal F.',
        firstName: 'Crystal',
        lastName: 'Farnsworth',
        emailAddress: 'twoagain@earthlink.net',
        externalId: null,
        city: 'Florence',
        state: 'OR',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 769,
      rating: 5,
      title: 'Blue blue blue suede shoes',
      text:
        'Love my blue suede shoes!  Suede version of the Felize is a bit more snug in the toe box than the smooth leather version.  Color is very rich!',
      textLength: 142,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Narrow']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T15:02:50-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T10:56:53-05:00',
      user: {
        nickName: 'Kendal C.',
        firstName: 'Kendal',
        lastName: 'Cornwall',
        emailAddress: 'kina1962@aol.com',
        externalId: null,
        city: 'Dallas',
        state: 'TX',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 752,
      rating: 5,
      title: 'Love them so much!',
      text:
        'I have literally worn these shoes every day since I bought them! I first made the purchase to give my Tods loafers a break, but now I never even wear them and only wear these! They are so cute and I get compliments every day. Amazing value and so comfortable! I had to go buy another color because I’m worried people are going to think I only own one pair of shoes, so now I’ve got these in two colors. I’m likely going to own the whole collection by years end... I love them that much! \nPlus, the customer service and all the people who work at M. Gemi are so nice, you can’t really go wrong with a fabulous comfy shoe (that goes so great with jeans), amazing quality, great customer service and really nice attention to little details like packaging and the stitching on the shoes. I cannot say enough great things here... but these shoes!',
      textLength: 841,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T15:12:19-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T09:48:25-05:00',
      user: {
        nickName: 'Carrie U.',
        firstName: 'Carrie',
        lastName: 'Ucer',
        emailAddress: 'csalt7@gmail.com',
        externalId: null,
        city: 'Beverly Hills',
        state: 'CA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 733,
      rating: 5,
      title: 'Go With Everything Shoes',
      text:
        'I was a little hesitant to buy these shoes, having seen them only on line, but I am now a believer! I wear them all the time -they look beautiful with everything and are soft and comfortable. I will say that they are a little narrow until you have worn them a while, but I could wear them all day without discomfort. I bought the grey but am considering other colors for spring.',
      textLength: 378,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Narrow']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:14:03-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T08:11:43-05:00',
      user: {
        nickName: 'Melissa M.',
        firstName: 'Melissa',
        lastName: 'Magowan',
        emailAddress: 'tazmama99@aol.com',
        externalId: null,
        city: 'Yorktown',
        state: 'VA',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 728,
      rating: 5,
      title: 'Love The Felize',
      text:
        'This was my first pair.   I think this is going to be a wardrobe basic for years to come.  Beautiful workmanship and not as boxy and wide as other drivers out there lately.  Love the price and the selection of colors.',
      textLength: 217,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Narrow']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:23:22-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T07:07:30-05:00',
      user: {
        nickName: 'Sutton',
        firstName: 'Ellen',
        lastName: 'Sutton',
        emailAddress: 'esutton@centurylink.net',
        externalId: null,
        city: 'Beaufort',
        state: 'SC',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 715,
      rating: 5,
      title: 'Super comfy shoes',
      text:
        'These shoes feel wonderful. Can wear them all day easily. Plus they look great!',
      textLength: 79,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:30:10-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T01:17:40-05:00',
      user: {
        nickName: 'Marion B.',
        firstName: 'Marion',
        lastName: 'Buisson',
        emailAddress: 'redbuisson@gmail.com',
        externalId: null,
        city: 'Gainesville',
        state: 'GA',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 710,
      rating: 5,
      title: 'The most comfortable shoe that I have bought in a loooong time!',
      text:
        "The Felize has an amazing fit. It seemed a little ang at first but after wearing just once the fit was PERFECT. \nIt iscsonsoft and comfortable. \nI'm deciding what color to get in leather for spring.",
      textLength: 198,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:41:44-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T00:54:55-05:00',
      user: {
        nickName: 'Pat B.',
        firstName: 'Pat',
        lastName: 'Berger',
        emailAddress: 'pbastyle@gmail.com',
        externalId: null,
        city: 'Santa Fe',
        state: 'NM',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 704,
      rating: 5,
      title: 'Super comfortable',
      text:
        'Love the sleek sophisticated look of this shoe. I ordered them in blue and black- kept both. They look great with skinny jeans, crop pants and skirts. Very comfortable!!',
      textLength: 169,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:43:17-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-16T00:16:26-05:00',
      user: {
        nickName: 'Claire G.',
        firstName: 'Claire',
        lastName: 'Grossman',
        emailAddress: 'cgrossman818@gmail.com',
        externalId: null,
        city: 'Hollywood',
        state: 'FL',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 699,
      rating: 5,
      title: 'The Best Moccasins',
      text:
        'Shoe runs small but online customer service provides help with identifying appropriate size. Shoes are extremely comfortable and very stylish.  Quality is excellent! This is my third pair of M Gemi moccasins and I have no regrets.',
      textLength: 230,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Full Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:44:22-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-15T23:53:28-05:00',
      user: {
        nickName: 'Trisha N.',
        firstName: 'Trisha',
        lastName: 'Neasman',
        emailAddress: 'trishaneasman@yahoo.com',
        externalId: null,
        city: 'inverness',
        state: 'FL',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 696,
      rating: 3,
      title: 'Love everything about them but the fit is crazy small',
      text:
        'I wear a 36 (or 6/6.5) in every shoe always no matter what! All said and done, I settled on a 37.5. I like to wear hidden PDFs in my loafers to keep them longer, but still a big departure from my norm. Probably could have done the 37, but they felt like they pulled a bit when I walked and this sensation made my feet sore after a short while. Otherwise beautiful construction and material; on par with TODS. Navy beautiful!',
      textLength: 424,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Full Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Narrow']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T08:45:25-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-15T23:48:55-05:00',
      user: {
        nickName: 'Laura B.',
        firstName: 'Laura',
        lastName: 'Baker',
        emailAddress: 'laura.westerhold@gmail.com',
        externalId: null,
        city: 'New york',
        state: 'NY',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 691,
      rating: 5,
      title: 'Perfect shoe',
      text:
        'Lovely fit & shoe, a shoe everyone should own. I now want a leather pair of driver flats.',
      textLength: 89,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T15:18:01-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-15T23:39:12-05:00',
      user: {
        nickName: 'Laurie B.',
        firstName: 'Laurie',
        lastName: 'Bova',
        emailAddress: 'lsbmob@aol.com',
        externalId: null,
        city: 'Boca Raton',
        state: 'FL',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 684,
      rating: 5,
      title: 'Beautiful Shoes',
      text:
        'These shoes are so comfy! I was hesitant to buy them at first, but they come true to size and looks stylish with everything. I will definitely buy more!',
      textLength: 152,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-16T15:21:48-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-15T23:05:12-05:00',
      user: {
        nickName: 'Helen A.',
        firstName: 'Helen',
        lastName: 'Au',
        emailAddress: 'helendoau@gmail.com',
        externalId: null,
        city: null,
        state: null,
        country: null,
        ageRange: null,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 679,
      rating: 5,
      title: 'Be kind to your feet',
      text:
        'The Felize is easily the most comfortable shoe I have ever owned. There is absolutely no break-in period...they are perfect right out of the beautiful box they arrived in! I highly recommend these shoes!',
      textLength: 203,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T22:36:04-05:00',
      user: {
        nickName: 'Lois C.',
        firstName: 'Lois',
        lastName: 'Cole',
        emailAddress: 'loiswcole@gmail.com',
        externalId: null,
        city: 'Deerfield',
        state: 'VA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 670,
      rating: 5,
      title: 'Great suede driving mocs',
      text:
        'These are great suede driving moccasins. Chic and streamlined, they are great with jeans. Hey Very comfortable and well made.',
      textLength: 125,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T22:07:41-05:00',
      user: {
        nickName: 'Rebecca M.',
        firstName: 'Rebecca',
        lastName: 'Mark',
        emailAddress: 'rebeccamark@gmail.com',
        externalId: null,
        city: 'San Francisco',
        state: 'CA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 661,
      rating: 5,
      title: 'Great, comfortable and chic',
      text:
        'I got these because I wanted something that I could slip on as I run out the door in the morning. Love! They look great and fit like a glove.',
      textLength: 141,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T21:53:04-05:00',
      user: {
        nickName: 'Alison C.',
        firstName: 'Alison',
        lastName: 'Chiu',
        emailAddress: 'apolley_chiu@yahoo.com',
        externalId: null,
        city: 'Los Angeles',
        state: 'CA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 659,
      rating: 5,
      title: 'Never taking them off',
      text:
        'I love these shoes. So comfortable and look great. Wearing them everywhere. I’m even considering another pair',
      textLength: 109,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T21:50:41-05:00',
      user: {
        nickName: 'Erin M.',
        firstName: 'Erin',
        lastName: 'Muckey',
        emailAddress: 'emuckey@gmail.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: null,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 638,
      rating: 5,
      title: 'LOVE  LOVE',
      text:
        "Absolutely Love these loafers; THE most comfortable shoe I have ever worn. More expensive loafers can't compare to the this  shoe. Already looking for a leather pair.",
      textLength: 166,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T20:41:50-05:00',
      user: {
        nickName: 'Teisa B.',
        firstName: 'Teisa',
        lastName: 'Binder',
        emailAddress: 'gailkbinder@aol.com',
        externalId: null,
        city: 'Houston',
        state: 'TX',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 636,
      rating: 5,
      title: 'Amazing!',
      text:
        'They are amazing! I love the way they fit and feel. They also look great! Totally met my expectations! Just beautifully made and worth every penny.',
      textLength: 147,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T20:32:34-05:00',
      user: {
        nickName: 'Rose C.',
        firstName: 'Rose',
        lastName: 'Coleman',
        emailAddress: 'colemanr46@gmail.com',
        externalId: null,
        city: 'Sparta',
        state: 'NJ',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 633,
      rating: 4,
      title: 'Felize review',
      text:
        "I would like to say that these shoes are the most comfortable item in my shoe collection. I usually wear a size 9.5 but due to European sizes a 10 is perfect.\nMy only issue is how to keep them clean.  The beige is a color that gets dirty easily and even though I wipe them off after wearing them they still have stains.\nThe other colors don't stain as badly but I do have the same issue.",
      textLength: 387,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T20:27:16-05:00',
      user: {
        nickName: 'Alicia H.',
        firstName: 'Alicia',
        lastName: 'Hille',
        emailAddress: 'ceces71@comcast.net',
        externalId: null,
        city: 'Paradise',
        state: 'CA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 620,
      rating: 5,
      title: 'Best shoes I own',
      text:
        'I ended up ordering a size 39 because the color I wanted was not available in 38.5. I’m glad I did go up as they are the perfect fit and have been unbelievably comfortable. I absolutely love them and will continue to spend splurge on the felize than get other shoes. They are incredibly comfortable!',
      textLength: 299,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Smaller']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T19:52:08-05:00',
      user: {
        nickName: 'Kathryn S.',
        firstName: 'Kathryn',
        lastName: 'Sheehan',
        emailAddress: 'kayla.sheehan22@gmail.com',
        externalId: null,
        city: 'Albany',
        state: 'NY',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 618,
      rating: 5,
      title: 'Perfecto',
      text:
        "Perfecto: Styling, fit, durability! Wish I could own in every color :) And, I also wish I\nrealized one has to act fast to grab a new offering as they get snapped up F A S T.\nEveryone should have one pair of Felize! I'm glad I do.",
      textLength: 229,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T19:48:55-05:00',
      user: {
        nickName: 'Heidi R.',
        firstName: 'Heidi',
        lastName: 'Riggs',
        emailAddress: 'hberiggs@gmail.com',
        externalId: null,
        city: 'Norwalk',
        state: 'CT',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 612,
      rating: 5,
      title: 'Love them!',
      text:
        "These shoes are so, so comfortable, fashionable, and I love the color. I wasn't sure if I would love the look from the pictures online, but I adore them in person. Great for work (I'm a teacher) or casual days around town. I do not typically spend this much for shoes (I tend to be more in the 30-70 range), but I do not regret buying them one bit!",
      textLength: 348,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T19:31:03-05:00',
      user: {
        nickName: 'Jennifer S.',
        firstName: 'Jennifer',
        lastName: 'Sano-Franchini',
        emailAddress: 'jsano81@gmail.com',
        externalId: null,
        city: 'Blacksburg',
        state: 'VA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 583,
      rating: 5,
      title: 'I’m A Believer',
      text:
        'I bought a pair of Felize in a perfect shade of navy. I live in them as they are comfortable and supportive. I’ve lost count of all the compliments I receive every time I’m out! \nThe craftsmanship is perfection and the style while casual shouts of  high end quality!\nGrazie M..Gemi',
      textLength: 281,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-15T19:27:44-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-15T18:25:11-05:00',
      user: {
        nickName: 'Linda B.',
        firstName: 'Linda',
        lastName: 'Bonerba',
        emailAddress: 'lindabonerba@gmail.com',
        externalId: null,
        city: 'Charlotte',
        state: 'NC',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 578,
      rating: 5,
      title: 'Very comfortable',
      text:
        'It is suggested that the shoe is snug at first, so I ordered the larger of my size range and it is perfect. The Felize is very comfortable, well made, and looks great. I am very happy with my first M. Gemi purchase.',
      textLength: 215,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T18:17:00-05:00',
      user: {
        nickName: 'Susan L.',
        firstName: 'Susan',
        lastName: 'Levick',
        emailAddress: 'susanlevick1@gmail.com',
        externalId: null,
        city: null,
        state: 'TX',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 571,
      rating: 5,
      title: 'Love them....',
      text:
        'I love the Felize because it is Comfortable, stylish and a perfect fit.',
      textLength: 71,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T18:06:29-05:00',
      user: {
        nickName: 'Laurie P.',
        firstName: 'Laurie',
        lastName: 'Phillips-Cox',
        emailAddress: 'phillipscoxlaurie@yahoo.com',
        externalId: null,
        city: 'Raynham',
        state: 'MA',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 555,
      rating: 5,
      title: 'Great!',
      text: 'Love these shoes, I recommend them without a doubt',
      textLength: 50,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: false,
      autoModerated: false,
      published: false,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: null,
      purchaseDate: null,
      dateCreated: '2018-02-15T17:44:54-05:00',
      user: {
        nickName: 'Claudia C.',
        firstName: 'Claudia',
        lastName: 'Canales',
        emailAddress: 'ccsf2nyc@gmail.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    }
  ]
};

/* Limit 6 - no offset */

const reviewDataPageLimit = {
  offset: 0,
  limit: 6,
  total: 41,
  reviews: [
    {
      id: 1000,
      rating: 5,
      title: 'Love these shoes!',
      text:
        'These shoes are extremely comfortable and look really nice.  Extra bonus that they come with a personalized card and dust bag for carrying.  Looking forward to buying my next pair!',
      textLength: 180,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-23T11:35:36-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-23T01:26:27-05:00',
      user: {
        nickName: 'Andrea L.',
        firstName: 'Andrea',
        lastName: 'Loh',
        emailAddress: 'aloh@umich.edu',
        externalId: null,
        city: 'SAN FRANCISCO',
        state: 'CA',
        country: null,
        ageRange: 1,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 999,
      rating: 5,
      title: 'Love, Love, Love',
      text:
        'This shoe is fabulous. Not only is it beautifully designed and fabricated, it is extremely comfortable and looks beautiful on your foot. Every time I wear them I receive compliments.\nI  love this shoe so much, I recently purchased 5 pairs. Once you try them you’ll be hooked as well. \nP.S.  Their Customer Service department also rocks - friendly and helpful. Packaging  is also pristine and they ship out immediately. So glad I discovered this brand.\n\nDebbie K.',
      textLength: 462,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-23T11:35:44-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-23T01:00:45-05:00',
      user: {
        nickName: 'Steven A.',
        firstName: 'Steven',
        lastName: 'Alessio',
        emailAddress: 'debbie.koenigsberg@gmail.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 962,
      rating: 5,
      title: 'Beautiful Italian shoes!!!',
      text:
        "From the careful packaging and shoe bag to the wonderful aroma of fine leather and artisanal shoemaking that greeted me when I opened the box, I was completely smitten! These are by far the nicest shoes (including Ferragamo) that I've bought in a long time. Fit perfectly and luxurious materials. Will be buying more!",
      textLength: 317,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T13:58:10-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-20T11:08:18-05:00',
      user: {
        nickName: 'sylvia c.',
        firstName: 'sylvia',
        lastName: 'calabrese',
        emailAddress: 'sylvia.calabrese@mac.com',
        externalId: null,
        city: 'New York',
        state: 'NY',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 960,
      rating: 5,
      title: 'comfy',
      text:
        "These shoes are very comfortable and durable.\nMy daughter saw mine and loved them, so I bought another pair.\nYou won't be disappointed!",
      textLength: 135,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt True to Size']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:16:54-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-20T08:47:55-05:00',
      user: {
        nickName: 'CHRISTINE M.',
        firstName: 'CHRISTINE',
        lastName: 'MARKEY',
        emailAddress: 'tinemarkey@gmail.com',
        externalId: null,
        city: 'ASPEN',
        state: 'CO',
        country: null,
        ageRange: 2,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 954,
      rating: 4,
      title: 'Nice, but need more cush.',
      text:
        'I love the look of the Felize shoe.  It’s comfortable to an extent. However, it needs a little more cushion on the inside.  I have morton’s neuroma on one of my feet and it is terribly irritated after wearing this shoe for a few hours. I had to address my own cushion inside.',
      textLength: 275,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt Slightly Wide']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T10:17:09-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T20:53:28-05:00',
      user: {
        nickName: 'Gena C.',
        firstName: 'Gena',
        lastName: 'Cofer',
        emailAddress: 'genacofer@gmail.com',
        externalId: null,
        city: 'Edisto Island',
        state: 'SC',
        country: null,
        ageRange: 3,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    },
    {
      id: 953,
      rating: 4,
      title: 'Wife loves them!',
      text:
        'Got these as a gift for my wife. Ran a half size big, so we did an exchange. Process was simple and fast. Customer service is great. She says they’re very comfortable and is very happy with them.',
      textLength: 195,
      locale: 'en_US',
      csFlag: false,
      inappropriateFlag: false,
      reviewedFlag: true,
      autoModerated: false,
      published: true,
      upVotes: 0,
      downVotes: 0,
      incentivized: false,
      orderId: null,
      catalogItems: [
        {
          sku: '03_1000_22',
          title: 'The Felize',
          url: 'http://mgemi.com/moccasins/the-felize/03_1000_22.html',
          category: null,
          reviewCount: 41,
          ratingCount: 41,
          averageRating: 4.8,
          ratingBreakdown: {
            '1': 0,
            '2': 0,
            '3': 2,
            '4': 4,
            '5': 35
          }
        }
      ],
      dimensions: [
        {
          type: 3,
          dimensionId: 3,
          dimensionLabel: 'Size',
          label: ['Felt Half Size Larger']
        },
        {
          type: 3,
          dimensionId: 2,
          dimensionLabel: 'Width',
          label: ['Felt True to Width']
        },
        {
          type: 3,
          dimensionId: 1,
          dimensionLabel: 'Would you recommend this shoe?',
          label: ['YES']
        }
      ],
      media: {
        photo: [],
        video: [],
        audio: []
      },
      responses: [],
      reviewedDate: '2018-02-20T09:59:16-05:00',
      purchaseDate: null,
      dateCreated: '2018-02-19T19:52:11-05:00',
      user: {
        nickName: 'Roger W.',
        firstName: 'Roger',
        lastName: 'Wong',
        emailAddress: 'baronvonwong@gmail.com',
        externalId: null,
        city: null,
        state: null,
        country: null,
        ageRange: null,
        badge: null,
        shopperProfiles: []
      },
      tags: []
    }
  ]
};
