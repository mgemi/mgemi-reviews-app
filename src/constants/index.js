export const SIZE_OPTIONS = [
  'Felt Full Size Smaller',
  'Felt Half Size Smaller',
  'Felt True to Size',
  'Felt Half Size Larger',
  'Felt Full Size Larger'
];

export const WIDTH_OPTIONS = [
  'Felt Narrow',
  'Felt Slightly Narrow',
  'Felt True to Width',
  'Felt Slightly Wide',
  'Felt Wide'
];
