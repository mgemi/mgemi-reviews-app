import React from 'react';

import Button from '../../components/Button';
import FloatingLabel from '../../components/FloatingLabel';
import Tooltip from '../../components/Tooltip';

import { createQuestion } from '../../common/questionsUtils';

import './QuestionList.css';

class QuestionList extends React.Component {
  state = {
    questions: [],
    text: '',
    name: '',
    email: '',
    questionError: '',
    emailError: '',
    loading: false
  };

  handleChange = event => {
    if (event.target.value !== '') {
      this.setState({
        [event.target.name + 'Error']: ''
      });
    }

    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = () => {
    const { loading, text, name, email } = this.state;

    if (!text.length) {
      this.setState({
        questionError: 'Please enter a question.'
      });
      return;
    }

    if (loading) return;

    this.setState({
      loading: true
    });

    createQuestion({
      text,
      name,
      email
    }).then(() => {
      this.setState({
        loading: false,
        questionError: '',
        text: '',
        name: '',
        email: ''
      });

      const questionSubmitEvent = new Event('productQuestionsSubmitted');
      window.dispatchEvent(questionSubmitEvent);
    });
  };

  render() {
    const { questionError, loading } = this.state;
    const inputWidth = window.innerWidth < 768 ? '96%' : '60%';

    return (
      <div className="question-list">
        <p>
          Currently there are no questions about these shoes. Be the first to
          ask our fit experts.
        </p>

        <FloatingLabel
          type="textarea"
          htmlId="text"
          name="text"
          placeholder="Your Question"
          error={questionError}
          width={inputWidth}
          height={'150px'}
          onChange={e => this.handleChange(e)}
          value={this.state.text}
        />

        <FloatingLabel
          type="text"
          htmlId="name"
          name="name"
          placeholder="Name"
          width={inputWidth}
          onChange={e => this.handleChange(e)}
          value={this.state.name}
        />

        <FloatingLabel
          type="email"
          htmlId="email"
          name="email"
          placeholder="Email Address"
          width={inputWidth}
          onChange={e => this.handleChange(e)}
          value={this.state.email}
        />

        <Button
          htmlId="submit-button"
          name="submit-button"
          width={inputWidth}
          isLoading={loading}
          onClick={e => this.handleSubmit(e)}
        >
          Submit
        </Button>

        <span className="disclaimer">
          By clicking submit you agree to these{' '}
          <Tooltip label="terms" target="qa-form">
            By clicking submit, you agree to allow M.Gemi to display your Name
            from this form with your question. M.Gemi will never publish your
            personal contact information.
          </Tooltip>
        </span>
        <hr />
      </div>
    );
  }
}

export default QuestionList;
