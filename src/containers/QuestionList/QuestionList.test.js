import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import * as setupTests from '../../setupFiles';
import fetchMock from 'fetch-mock';

import QuestionList from './QuestionList';
// import { questionData } from '../../config/data';

describe('QuestionList component', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  const wrapper = shallow(<QuestionList />);

  // fetchMock.getOnce(
  //   '/s/mgemi/turntoget/?api=questions&parametermap=sku%3D03_1000_22',
  //   questionData
  // );

  it('renders without crashing', () => {
    shallow(<QuestionList />);
  });

  it('renders the component correctly', () => {
    const tree = renderer.create(<QuestionList />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders the correct HTML', () => {
    expect(wrapper.find('div').first()).toHaveClassName('.question-list');
    // console.log(wrapper.debug());
  });
});
