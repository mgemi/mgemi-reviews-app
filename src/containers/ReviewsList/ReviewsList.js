import React from 'react';

import Review from '../../components/Review';
import Rating from '../../components/Rating';
import Pagination from '../../components/Pagination';

import { requestReviews } from '../../common/reviewsUtils';

import './ReviewList.css';

class ReviewsList extends React.Component {
  state = {
    reviewData: {},
    pageOfItems: [],
    paging: {
      pageSize: 6,
      offset: 0
    }
  };

  // Fetch me some reviews
  componentDidMount() {
    window.app = window.app || {};
    window.app.page = window.app.page || {};

    const reviewsEvent = new Event('productReviewsLoaded');
    const cachedData = this.onGetStorage('reviewData-' + window.app.page.id);

    // Check if the data is in sessionStorage and use that if yes
    if (cachedData) {
      this.setState({
        reviewData: JSON.parse(cachedData)
      });
      return;
    }

    debugger;

    requestReviews(this.state.paging)
      .then(reviewData => {
        if (!reviewData.data.errors || !reviewData.data.errors.length) {
          // Store the data in sessionStorage
          this.onSetStorage(reviewData, 'reviewData-' + window.app.page.id);

          this.setState({
            reviewData: reviewData
          });

          window.dispatchEvent(reviewsEvent);
        } else {
          // Store the empty data in sessionStorage
          this.onSetStorage(null, 'reviewData-' + window.app.page.id);

          this.setState({
            reviewData: null
          });
        }
      })
      .catch(err => {
        console.error(err.message);
      });
  }

  onGetStorage = value => {
    const cachedData = sessionStorage.getItem(value);
    if (cachedData) {
      return cachedData;
    }
    return false;
  };

  onSetStorage = (data, key) => {
    sessionStorage.setItem(key, JSON.stringify(data));
  };

  // Handle page change and send request for review data
  onChangePage = (pager, isInitialPage) => {
    if (!isInitialPage) {
      const reviewsEvent = new CustomEvent('productReviewsPageChange', {
        detail: {
          page: pager.currentPage
        }
      });

      window.dispatchEvent(reviewsEvent);
    }

    this.setState(
      {
        paging: {
          pageSize: pager.pageSize,
          offset: (pager.currentPage - 1) * pager.pageSize
        }
      },
      () => {
        requestReviews(this.state.paging)
          .then(reviewData => {
            this.setState({
              pageOfItems: reviewData.data.reviews
            });
          })
          .catch(err => {
            console.error(err.message);
          });
      }
    );
  };

  render() {
    const { reviewData, pageOfItems, paging } = this.state;

    if (reviewData === null) {
      return null;
    }

    if (
      !reviewData ||
      !reviewData.data ||
      !reviewData.data.hasOwnProperty('reviews')
    ) {
      return <div className="loading" />;
    }

    if (reviewData && reviewData.data && !reviewData.data.total) {
      return null;
    }

    // Add the rating average and total count to window.app.productReviews
    if (
      reviewData.data.reviews[0] &&
      reviewData.data.reviews[0].catalogItems[0]
    ) {
      window.app = window.app || {};
      window.app.productReviews = window.app.productReviews || {};
      window.app.productReviews.rating =
        reviewData.data.reviews[0].catalogItems[0].averageRating;
      window.app.productReviews.totalRatings =
        reviewData.data.reviews[0].catalogItems[0].ratingCount;
    }

    return (
      <section className="reviews-list">
        <header className="reviews-header">
          {reviewData.data.reviews[0] &&
            reviewData.data.reviews[0].catalogItems[0] && (
              <Rating
                rating={
                  reviewData.data.reviews[0].catalogItems[0].averageRating
                }
                totalRatings={reviewData.data.total}
                align="center"
                size={25}
                showAverage
              />
            )}

          <p className="reviews-about">
            The following reviews are written by M.Gemi customers who have
            purchased the product. Here you can compare how customers of
            different ages, foot profiles and sizes rate this shoe in respect to
            fit comfort, quality and style. When you purchase a shoe from us, we
            will reach out and ask you for your thoughts!
          </p>
        </header>

        <Review items={pageOfItems} />

        <Pagination
          total={reviewData.data.total}
          onChangePage={(pager, isInitialPage) =>
            this.onChangePage(pager, isInitialPage)
          }
          pageSize={paging.pageSize}
          offset={paging.offset}
        />
      </section>
    );
  }
}

ReviewsList.propTypes = {};

export default ReviewsList;
