import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import * as setupTests from '../../setupFiles';
import fetchMock from 'fetch-mock';

import ReviewsList from './ReviewsList';
import { reviewData } from '../../config/data';

describe('ReviewsList component', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  const wrapper = shallow(<ReviewsList />);

  fetchMock.getOnce(
    '/s/mgemi/turntoget/?api=reviews&parametermap=sku%3D03_1000_22%26includerelated%3Dtrue%26limit%3D6%26offset%3D0',
    reviewData
  );

  it('renders without crashing', () => {
    shallow(<ReviewsList />);
  });

  it('renders the component correctly', () => {
    const tree = renderer.create(<ReviewsList />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders the correct HTML', () => {
    expect(wrapper.find('div').first()).toHaveClassName('.loading');
    // console.log(wrapper.debug());
    // expect(wrapper.find('section').first()).toHaveClassName('.reviews-list');
  });
});
