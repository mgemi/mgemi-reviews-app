import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';

const rootEl =
  process.env.NODE_ENV === 'development' ? 'root' : 'product-reviews';
ReactDOM.render(<App />, document.getElementById(rootEl));
